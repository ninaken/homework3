package com.example.example

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {

    private lateinit var editTextPersonName: EditText;
    private lateinit var button:Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Log.d("MyData", "")

        editTextPersonName=findViewById(R.id.editTextPersonName)
        button=findViewById(R.id.button)

        button.setOnClickListener{
            val name =editTextPersonName.text.toString()
            val intent= Intent(this, SecondActivity2 ::class.java)
            intent.putExtra("NAME",name)
            startActivity(intent)



        }

    }
    override fun onStart(){
        super.onStart()
        Log.d("MyData", "")
    }


    override  fun onResume(){
        super.onResume()
        Log.d("MyData", "")
    }

    override fun onPause(){
        super.onPause()
        Log.d("MyData", "")
    }
    override fun onStop(){
        super.onStop()
        Log.d("MyData", "")
    }

    override fun onRestart() {
        super.onRestart()
        Log.d("MyData", "")
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        Log.d("MyData", "")
    }



}