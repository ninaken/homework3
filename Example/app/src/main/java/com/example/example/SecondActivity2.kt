package com.example.example

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class SecondActivity2 : AppCompatActivity() {
    private lateinit var finishButton:Button
    private  lateinit var ageEditText:EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second2)

        finishButton=findViewById(R.id.button2)
        ageEditText=findViewById(R.id.editTexage)

        val name:String? =intent.extras?.getString("NAME","noname")

        finishButton.setOnClickListener{
            val ageText=ageEditText.text.toString()

            if (ageText.isNullOrEmpty()){
                return@setOnClickListener
            }

            val age=ageText.toInt()
            val intent = Intent(this, thirdactivity::class.java)
            intent.putExtra("AGE",age)
            startActivity(intent)



        }

    }
}