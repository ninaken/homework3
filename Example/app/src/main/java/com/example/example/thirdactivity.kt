package com.example.example

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class thirdactivity : AppCompatActivity() {

    private lateinit var finishButton: Button
    private lateinit var ageEditText: EditText
    private lateinit var textView: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_thirdactivity2)


        finishButton = findViewById(R.id.button3)
        ageEditText = findViewById(R.id.editTextTextPersonName2)
        textView = findViewById(R.id.textView2)
        val name: String? = intent.extras?.getString("ZODIAC", "noname")

        finishButton.setOnClickListener {
            val ageText = ageEditText.text.toString()


            val intent = Intent(this, FinishActivity::class.java)

            intent.putExtra("ZODIAC",name)
            startActivity(intent)


        }
    }
}